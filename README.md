# Простой web crawler

На вход подается список доменных имен.
Приложение пытается получить для них `title`, `meta description`, `meta keywords`.

# Сборка через sbt

Для сборки используется плагин `sbt-assembly`

В `build.sbt` внесены настройки, чтобы следующая команда упаковывала в `JAR` только классы внутри проекта. В результате создается файл `target\scala-2.12\toolbox-assembly-1.0.jar`
```
sbt assembly
```

Дополнительно нужно выполнить команду для создания `fat JAR`, в который будут помещены все внешние библиотеки (`akka`, `logback`). В результате создается файл `target\scala-2.12\toolbox-assembly-1.0-deps.jar`
```
sbt assemblyPackageDependency
```

Внутри `fat JAR` размещается `reference.conf`, где хранятся настройки по умолчанию.
Но файл `application.conf` с основными настройками и `logback.xml` нужно предоставлять самостоятельно при запуске приложения.

На `windows` в самом простом случае можно использовать команду

```
java -Dconfig.file=application.conf -cp ".;toolbox-assembly-1.0.jar;toolbox-assembly-1.0-deps.jar" com.toolbox.web.crawler.Main
```

Если `linux`, то команду нужно поменять

```
java -Dconfig.file=application.conf -cp ".:toolbox-assembly-1.0.jar:toolbox-assembly-1.0-deps.jar" com.toolbox.web.crawler.Main
```

# Описание конфигурационного файла

Основные настройки хранятся в `application.conf`

```
web-crawler {
  read-from = ".\\result\\input\\short.txt"
  write-to = ".\\result\\output\\other_meta.json"
  
  parallelism = 100

  connection-timeout = 5 seconds
  connection-idle-timeout = 5 seconds
}
```

* `web-crawler.read-from` - Путь до файла со списком доменных имен. В качестве разделителя используется новая строка. Желательно конвертировать файл в кодировку `UTF-8`
* `web-crawler.write-to` - Путь к файлу, в который будут помещать результаты по мере их поступления
* `web-crawler.parallelism` - Сколько запросов к разным хостам будет осуществляться одновременно
* `web-crawler.connection-timeout` - Время ожидания соединения
* `web-crawler.connection-idle-timeout` - Время в течении которого не происходит передача данных от сервера. После чего соединение прерывается.

В файле `reference.conf` хранятся настройки по умолчанию. Для отладки включено логирование `akka`
```
akka {
  loggers = ["akka.event.slf4j.Slf4jLogger"]
  loglevel = "DEBUG"
  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
}
```

# Формат для хранения результатов

Для записи данных используется `JSON`. Чтобы полученные данные при необходимости можно было потом прочитать. Например, такая запись может иметь вид
```
{
"endpoint": "7jigen.net",
"http": [{
  "status": "200 OK",
  "title": ["7jigen.net"],
  "keywords": ["7jigen, heavenlyblue, alien"],
}],
"https": [{
  "status": "500 Internal Server Error",
  "error": "General SSLEngine problem",
}],
}
```

При обращении к `http://7jigen.net` удалось получить `title`, `keywords`.
Но не получилось соединиться при обращении к `https://7jigen.net`

# Замечания по реализации

* Некоторые сайты могут быть доступны по `HTTPS`, но не доступны по `HTTP`. Или наоборот. Поэтому запрос к одному домену делается без шифрования и с шифрованием. Полученные результаты записываются вместе.

* Для анализа полученных `HTML` старниц используется написанный от руки парсер `SimpleHTMLParser`. Это сделано для обеспечения простоты. Можно попробовать использовать регулярные выражения. Но их лучше для HTML не использовать, они легко ломаются и не масштабируются, когда нужно искать что-то сложное.

* Судя по всему `akka-http` считает невалидными доменные имена, которые содержат буквы отличные от латинских. Поэтому не удается сделать запросы с использованием доменных имен на русском.

