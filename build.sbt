name := "toolbox"
version := "1.0"
scalaVersion := "2.12.10"
mainClass := Some("com.toolbox.web.crawler.Main")

test in assembly := {}
assemblyMergeStrategy in assembly := {
  case "logback.xml" => MergeStrategy.discard
  case "application.conf" => MergeStrategy.discard
  case PathList(ps@_*) if ps.last endsWith ".sql" => MergeStrategy.discard
  case PathList("org", "slf4j", xs@_*) => MergeStrategy.last
  case x => (assemblyMergeStrategy in assembly).value(x)
}
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false, includeDependency = false)

val akkaVersion = "2.5.4"
val akkaHttpVersion = "10.0.10"

lazy val scalatestVersion = "3.0.5"
lazy val scalaMockVersion = "4.1.0"

lazy val scalatestDependencies = Seq(
  "org.scalatest" %% "scalatest" % scalatestVersion % Test
)

lazy val scalaMockDependencies = Seq(
  "org.scalamock" %% "scalamock" % scalaMockVersion % Test
)

libraryDependencies ++= scalatestDependencies
libraryDependencies ++= scalaMockDependencies

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"


libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  "com.typesafe.akka" %% "akka-http-core" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion
)


