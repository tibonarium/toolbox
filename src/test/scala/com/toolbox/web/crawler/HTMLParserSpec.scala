package com.toolbox.web.crawler

import org.scalatest.FlatSpec
import SimpleHTMLParser._
import Model._
import akka.http.scaladsl.model.StatusCodes
import org.slf4j.LoggerFactory

class HTMLParserSpec extends FlatSpec {
  val logger = LoggerFactory.getLogger(this.getClass)

  val input = s"""
    <head>
        <title>FishComm.Ru</title>
        <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1"/>
        <meta name="description" content="D_FishComm" />
        <meta name="keywords" content="K_FishComm" />
  """

  val input2 = s"""
       <!DOCTYPE HTML>
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <title>FishComm.Ru: Рыболовный интернет магазин FishComm Shop, Рыболовное сообщество FishComm, форум FishComm</title>
          <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1"/>
          <meta name="description" content="Рыболовный сайт FishComm в настоящее время является лишь отправной точкой на всевозможные рыбацкие проекты, объединенные общим названием – ФишКомм." />
          <meta name="keywords" content="FishComm, FishComm.Ru, Рыболовный интернет магазин FishComm Shop, Рыболовный магазин FishComm Shop, блоги, видео, ФишКомм, рыболовный магазин, магазин, воблеры, блесны, силикон, спиннинги, леска, одежда, инструменты, контакты, доставка, Рыболовное сообщество FishComm, снасти для спиннинга, Удилища для спиннинга, кастинговые спиннинги, плетеные шнуры, Рыболовная одежда, силиконовые приманки, " />
          <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
  """

  it should "trim spaces and quotes" in {
    val input = """ "description" """
    val expecting = "description"
    val result = trimSpaceQuote(input)
    logger.info(s"result: `$result`")

    assert(result == expecting, "trimmed version should not contain quotes and spaces")
  }

  it should "detect port" in {
    val first_endpoint = "fkis.ru"
    val second_endpoint = "fkis.ru:3000"

    val first_http = toHttp(first_endpoint)
    val second_http = toHttp(second_endpoint)

    val first_https = toHttps(first_endpoint)
    val second_https = toHttps(second_endpoint)

    assert(first_http.authority.port == 80, "first http")
    assert(second_http.authority.port == 3000, "second http")

    assert(first_https.authority.port == 443, "first https")
    assert(second_https.authority.port == 3000, "second https")
  }

  it should "collect metadata from HTML" in {
    val parser = new SimpleHTMLParser(HTTPS, StatusCodes.OK)
    val expecting = Meta(
      scheme = HTTPS,
      status = StatusCodes.OK,
      title = List(Title("FishComm.Ru")),
      description = List(Description("D_FishComm")),
      keywords = List(Keywords("K_FishComm")))

    parser.parse(input)
    val meta = parser.getMeta
    logger.info(s"meta: $meta")

    assert(expecting == meta, "metadata was retrieved")
  }

  it should "collect metadata from complex HTML" in {
    val parser = new SimpleHTMLParser(HTTPS, StatusCodes.OK)

    parser.parse(input2)
    val meta = parser.getMeta
    logger.info(s"complex meta: $meta")

    assert(true, "ok")
  }
}
