package com.toolbox.web.crawler

import akka.http.scaladsl.model.StatusCode
import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import org.slf4j.LoggerFactory

import scala.collection.mutable.Stack
import Model._

object SimpleHTMLParser {
  val LT: Char = '<'
  val GT: Char = '>'
  val SPACE: Char = ' '
  val EQ: Char = '='
  val FORWARD_SLASH: Char = '/'
  val QUOTE: Char = '''
  val DOUDBLE_QUOTE: Char = '"'

  val TITLE_TAG = "title"
  val META_TAG = "meta"
  val NAME_ATTRIBUTE = "name"
  val CONTENT_ATTRIBUTE = "content"

  def displaySpecial(c: Char): String = c match {
    case '\n' => "<LF>"
    case '\r' => "<CR>"
    case '\t' => "<TAB>"
    case ' ' => "<SPACE>"
    case ch => ch.toString
  }

  def displaySpecial(input: String): String = {
    input.map(displaySpecial _).mkString("")
  }

  sealed trait Stage {
    def next(c: Char): Stage
    def content(): String
  }

  class Text() extends Stage {
    val contentBuffer = new StringBuffer()

    def content(): String = contentBuffer.toString()

    def next(c: Char): Stage = c match {
      case LT => new Tag(c)
      case ch => {
        contentBuffer.append(ch)
        this
      }
    }

    override def toString = s"Text(${displaySpecial(contentBuffer.toString())})"
  }

  class Tag(first: Char) extends Stage {
    var tagName = ""
    var tagNameCompleted = false
    var tagAttributes: Map[String, String] = Map.empty
    var currentAttribute = ""
    var closeTag = false
    var singleTag = false
    var insideQuote = false

    val contentBuffer = new StringBuffer()
    contentBuffer.append(first)
    val buffer = new StringBuffer()

    val pair: ((String, String)) => String = (t: (String, String)) => s"${t._1}=${t._2}"

    def name(): String = tagName
    def attributes(): Map[String, String] = tagAttributes
    def isClose(): Boolean = closeTag
    def isSingle(): Boolean = singleTag

    def clear() = {
      buffer.delete(0, buffer.length())
    }

    def content(): String = contentBuffer.toString()

    def next(c: Char): Stage = {
      contentBuffer.append(c)

      c match {
        case GT | SPACE => {
          if (insideQuote) { buffer.append(c) }
          else {

            if (tagNameCompleted == false) {
              tagName = trimSpaceQuote(buffer.toString())
              tagNameCompleted = true
            }
            else if (currentAttribute.nonEmpty) {
              val attrValue = trimSpaceQuote(buffer.toString())
              tagAttributes = tagAttributes + (currentAttribute -> attrValue)
              currentAttribute = ""
            }
            clear()

            if (c == GT) return new Text()
          }
        }
        case EQ => {
          currentAttribute = trimSpaceQuote(buffer.toString())
          clear()
        }
        case DOUDBLE_QUOTE => {
          if (insideQuote) {
            insideQuote = false
          } else {
            insideQuote = true
          }
        }
        case FORWARD_SLASH => {
          val previousChar: Option[Char] = if (contentBuffer.length() >= 2) {
            Some(contentBuffer.charAt( contentBuffer.length() - 2 ))
          } else { None }

          previousChar match {
            case Some(ch) if ch == LT => { closeTag = true }
            case _ => { singleTag = true }
          }
        }
        case ch => buffer.append(ch)
      }
      return this;
    }

    override def toString = {
      val close = if (closeTag) { ",close=true" } else { "" }
      val single = if (singleTag) { ",single=true" } else { "" }

      val attributes = tagAttributes.toSeq.map(pair).mkString(",")
      val attrs = if (attributes.nonEmpty) { s",attributes($attributes)" } else { "" }

      s"Tag(name=${tagName}${attrs}${close}${single})"
    }
  }

  def trimSpaceQuote(input: String): String = {
    trim(
      trim(
        trim(input, SPACE), DOUDBLE_QUOTE), QUOTE)
  }

  def trim(input: String, c: Char): String = {
    if (input.isEmpty) return input
    if (input.head == c || input.last == c) {
      val head = input.takeWhile(i => i == c).length()
      val tail = input.reverseIterator.takeWhile(i => i == c).count(p => true)
      input.drop(head).dropRight(tail)
    }
    else {
      input
    }
  }
}


class SimpleHTMLParser(scheme: Scheme, status: StatusCode) {
  import SimpleHTMLParser._

  val logger = LoggerFactory.getLogger(this.getClass)
  val stack = Stack[Stage]()
  var meta = Meta(scheme, status)

  def getMeta: Meta = meta

  def parse(input: String): Unit = {

    input.foldLeft[Stage](new Text())((stage: Stage, c: Char) => {
      val nextStage = stage.next(c)

      (stage, nextStage) match {
        case (text: Text, _: Tag) => {
          stack.push(text)
        }
        case (tag: Tag, _: Text) => {
          if (
            tag.isClose() &&
            stack.nonEmpty
          ) {
            val top = stack.top

            top match {
              case default: Text => {
                if (tag.name() == TITLE_TAG) {
                  meta = meta.copy(
                    title = meta.title :+ Title(default.content()))
                }
              }
              case _ => {}
            }
          } else if (!tag.isSingle()) {
            stack.push(tag)
          }

          if (tag.name() == META_TAG)
          {
            for(name <- tag.attributes().get(NAME_ATTRIBUTE)) name match {
              case "description" => for(content <- tag.attributes().get(CONTENT_ATTRIBUTE)) {
                meta = meta.copy(
                  description = meta.description :+ Description(content))
              }
              case "keywords" => for(content <- tag.attributes().get(CONTENT_ATTRIBUTE)) {
                meta = meta.copy(
                  keywords = meta.keywords :+ Keywords(content))
              }
              case _ =>
            }
          }
        }
        case _ => nextStage
      }
      nextStage
    })

  }
}


