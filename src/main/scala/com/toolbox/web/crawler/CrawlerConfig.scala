package com.toolbox.web.crawler

import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.TimeUnit

import com.typesafe.config.Config

import scala.concurrent.duration._

object Configuration {
  val WEB_CRAWLER_PREFIX = "web-crawler"

  def configOpt(path: String, config: Config): Option[Config] =
    Some(path).filter(config.hasPath).map(config.getConfig)

  def stringOpt(path: String, config: Config): Option[String] =
    Some(path).filter(config.hasPath).map(config.getString)

  def intOpt(path: String, config: Config): Option[Int] =
    Some(path).filter(config.hasPath).map(config.getInt)

  def durationOpt(path: String, config: Config): Option[Duration] = {
    Some(path)
      .filter(config.hasPath)
      .map(config.getDuration)
      .map(_.toNanos)
      .map(Duration.apply(_, TimeUnit.NANOSECONDS))
  }

  def finiteDurationOpt(path: String, config: Config): Option[FiniteDuration] = {
    Some(path)
      .filter(config.hasPath)
      .map(config.getDuration)
      .map(_.toNanos)
      .map(FiniteDuration.apply(_, TimeUnit.NANOSECONDS))
  }

  def apply(appConfig: Config): Configuration = Configuration(
    crawler = CrawlerConfig(configOpt(WEB_CRAWLER_PREFIX, appConfig)
      .getOrElse(throw new Exception(s"${WEB_CRAWLER_PREFIX} config was not found"))),
    connection = Connection(configOpt(WEB_CRAWLER_PREFIX, appConfig)
      .getOrElse(throw new Exception(s"${WEB_CRAWLER_PREFIX} config was not found"))))
}

case class Configuration(
                          crawler: CrawlerConfig,
                          connection: Connection)

object CrawlerConfig {
  import Configuration._

  def apply(config: Config): CrawlerConfig =
    CrawlerConfig(
      readFrom = stringOpt("read-from", config)
        .map(s => Paths.get(s))
        .getOrElse(throw new Exception(s"${WEB_CRAWLER_PREFIX}.read-from should be defined")),

      writeTo = stringOpt("write-to", config)
        .map(s => Paths.get(s))
        .getOrElse(throw new Exception(s"${WEB_CRAWLER_PREFIX}.write-to should be defined")),

      parallelism = intOpt("parallelism", config)
        .getOrElse(throw new Exception(s"${WEB_CRAWLER_PREFIX}.parallelism should be defined"))
    )
}

case class CrawlerConfig(
  readFrom: Path,
  writeTo: Path,
  parallelism: Int
)

object Connection {
  import Configuration._

  def apply(config: Config): Connection =
    Connection(
      timeout = finiteDurationOpt("connection-timeout", config)
      .getOrElse(throw new Exception(s"${WEB_CRAWLER_PREFIX}.connection-timeout should be defined")),

      idleTimeout = durationOpt("connection-idle-timeout", config)
      .getOrElse(throw new Exception(s"${WEB_CRAWLER_PREFIX}.connection-idle-timeout should be defined"))
    )
}


case class Connection(
  timeout: FiniteDuration,
  idleTimeout: Duration)