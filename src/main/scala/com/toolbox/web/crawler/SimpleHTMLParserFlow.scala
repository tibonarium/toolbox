package com.toolbox.web.crawler

import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import org.slf4j.LoggerFactory
import Model._
import akka.http.scaladsl.model.StatusCode

final class SimpleHTMLParserFlow(scheme: Scheme, status: StatusCode)
  extends GraphStage[FlowShape[String, Meta]] {

  val in = Inlet[String]("SimpleHTMLParserFlow.in")
  val out = Outlet[Meta]("SimpleHTMLParserFlow.out")
  override val shape = FlowShape(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) with InHandler with OutHandler {
      val parser = new SimpleHTMLParser(scheme, status)

      override def onPush(): Unit = {
        parser.parse(grab(in))
        pull(in)
      }

      override def onPull(): Unit = pull(in)

      def lastPush(): Unit = {
        val meta = parser.getMeta
        push(out, meta)
      }

      override def onUpstreamFinish() = {
        lastPush()
        super.onUpstreamFinish()
      }

      override def onUpstreamFailure(cause: Throwable) = {
        lastPush()
        super.onUpstreamFailure(cause)
      }

      setHandlers(in, out, this)
    }
}
