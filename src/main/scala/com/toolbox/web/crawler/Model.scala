package com.toolbox.web.crawler

import java.nio.charset.Charset

import akka.http.scaladsl.model.{HttpResponse, StatusCode, Uri}

object Model {
  val UTF_8 = Charset.forName("UTF-8")
  val LINE_SEPARATOR = System.lineSeparator() // "\r\n"
  val SIZE_LIMIT: Int = 5 * 1024 //  5 kB

  def toHttp(endpoint: String): Uri = {
    val items = endpoint.split(":")

    val port = if( items.length>1 && items(1).nonEmpty ) {
      try items(1).toInt
      catch { case ex: Throwable => 80 }
    } else { 80 }

    Uri(s"http://${endpoint}").withPort(port)


  }
  def toHttps(endpoint: String): Uri =  {
    val items = endpoint.split(":")

    val port = if( items.length>1 && items(1).nonEmpty ) {
      try items(1).toInt
      catch { case ex: Throwable => 443 }
    } else { 443 }

    Uri(s"https://${endpoint}").withPort(port)
  }

  case class Title(title: String)
  case class Description(description: String)
  case class Keywords(keywords: String)

  sealed trait Scheme
  case object HTTP extends Scheme
  case object HTTPS extends Scheme

  case class Meta(
                   scheme: Scheme,
                   status:   StatusCode,
                   title: List[Title] = Nil,
                   description: List[Description] = Nil,
                   keywords: List[Keywords] = Nil,
                   error: Option[String] = None)

  case class MetaResult(
                         endpoint: String,
                         http: List[Meta] = Nil,
                         https: List[Meta] = Nil)

  case class ConnectionResult(
                             response: HttpResponse,
                             error: Option[String] = None)

  def mkString[A](a: List[A], f: A => String): String =
    a.map(i => f(i)).mkString(",")

  def mkQuoteString[A](a: List[A], f: A => String): String =
    a.map(i => f(i)).map(s => "\"" + s + "\"").mkString(",")

  def metaToJson(meta: MetaResult): String = {
    val endpoint = meta.endpoint
    val http = mkString(meta.http, (m: Meta) => metaToJson(m))
    val https = mkString(meta.https, (m: Meta) => metaToJson(m))

    val sb = new StringBuilder()
    sb.append("{").append(LINE_SEPARATOR)
    sb.append(s""""endpoint": "${endpoint}",""").append(LINE_SEPARATOR)
    sb.append(s""""http": [${http}],""").append(LINE_SEPARATOR)
    sb.append(s""""https": [${https}],""").append(LINE_SEPARATOR)
    sb.append("},")

    sb.toString()
  }

  def metaToJson(meta: Meta): String = {
    val title = mkQuoteString(meta.title, (t: Title) => t.title)
    val description = mkQuoteString(meta.description, (d: Description) => d.description)
    val keywords = mkQuoteString(meta.keywords, (k: Keywords) => k.keywords)
    val status = meta.status.value

    val sb = new StringBuilder()
    sb.append("{").append(LINE_SEPARATOR)
    sb.append(s"""  "status": "${status}",""").append(LINE_SEPARATOR)
    if (meta.error.nonEmpty) {
      sb.append(s"""  "error": "${meta.error.getOrElse("")}",""").append(LINE_SEPARATOR)
    }

    if (title.nonEmpty) {
      sb.append(s"""  "title": [${title}],""").append(LINE_SEPARATOR)
    }
    if (description.nonEmpty) {
      sb.append(s"""  "description": [${description}],""").append(LINE_SEPARATOR)
    }
    if (keywords.nonEmpty) {
      sb.append(s"""  "keywords": [${keywords}],""").append(LINE_SEPARATOR)
    }

    sb.append("}")

    sb.toString()
  }
}
