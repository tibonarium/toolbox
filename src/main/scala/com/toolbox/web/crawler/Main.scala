package com.toolbox.web.crawler

import akka.http.scaladsl.{ClientTransport, Http}
import akka.http.scaladsl.model.{HttpEntity, HttpHeader, HttpMethods, HttpProtocol, HttpProtocols, HttpRequest, HttpResponse, ResponseEntity, StatusCode, StatusCodes, Uri}
import akka.http.scaladsl.settings.{ClientConnectionSettings, ConnectionPoolSettings}
import akka.stream.{BidiShape, Materializer}
import akka.stream.scaladsl.{BidiFlow, Concat, FileIO, Flow, Framing, GraphDSL, Keep, MergeHub, Sink, Source}
import akka.util.ByteString
import akka.event.{Logging, LoggingAdapter}

import scala.collection.immutable
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future, Promise, TimeoutException}
// akka
import akka.actor.{ ActorSystem, ActorRef, Props }
import akka.stream.ActorMaterializer
// logger
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

import SimpleHTMLParser._
import Model._

object Main extends App {
  val logger = LoggerFactory.getLogger(this.getClass)

  logger.info("Loading configuration...")
  val appConfig = ConfigFactory.load().resolve()
  val config = Configuration(appConfig)

  logger.info("Start...")
  complexFlow(config)

  def complexFlow(config: Configuration): Unit = {
    implicit val system = ActorSystem("request-stream")
    implicit val dispatcher: ExecutionContext = system.dispatcher
    implicit val actorMaterializer = ActorMaterializer()
    implicit val log: LoggingAdapter = Logging(system.eventStream, "crawler-logging")

    val timeout: FiniteDuration = config.connection.timeout
    val idleTimeout: Duration = config.connection.idleTimeout

    val defaultConnectionPoolSettings =
      ConnectionPoolSettings(system)

    val defaultClientConnectionSettings: ClientConnectionSettings =
      defaultConnectionPoolSettings.connectionSettings

    val poolSettings = defaultConnectionPoolSettings
      .withConnectionSettings(
        defaultClientConnectionSettings
          .withConnectingTimeout(timeout)
          .withIdleTimeout(idleTimeout))

    val source = FileIO.fromPath(config.crawler.readFrom)
      .via(Framing.delimiter(ByteString(LINE_SEPARATOR), Int.MaxValue))
      .map(bytes => bytes.decodeString(UTF_8))
      .log("endpoint")

    val sink = FileIO.toPath(config.crawler.writeTo)

    val sinkFlow = Flow[MetaResult]
      .map(meta => metaToJson(meta))
      .map(str => ByteString.apply(str, UTF_8))

    val graph = source
      .mapAsync(config.crawler.parallelism)((endpoint: String) => {

        val future1 = (Try(toHttps(endpoint)) match {
          case Success(https_uri) =>
            futureResponse(HTTPS, https_uri, poolSettings)
          case Failure(ex) =>
            Future.successful(
              Meta(
                scheme = HTTPS,
                status = StatusCodes.InternalServerError,
                error = Some(ex.getMessage())))
        }) transform  {
          case Success(value) => Success(value)
          case Failure(ex) => Success(
            Meta(
              scheme = HTTPS,
              status = StatusCodes.InternalServerError,
              error = Some(ex.getMessage())))
        }

        val future2 = (Try(toHttp(endpoint)) match {
          case Success(http_uri) =>
            futureResponse(HTTP, http_uri, poolSettings)

          case Failure(ex) =>
            Future.successful(
              Meta(
                scheme = HTTP,
                status = StatusCodes.InternalServerError,
                error = Some(ex.getMessage())))
        }) transform  {
          case Success(value) => Success(value)
          case Failure(ex) => Success(
            Meta(
              scheme = HTTP,
              status = StatusCodes.InternalServerError,
              error = Some(ex.getMessage())))
        }

        val result = for {
          meta1 <- future1;
          meta2 <- future2
        } yield {
          Seq(meta1, meta2).foldLeft(MetaResult(endpoint))((result, next) => next.scheme match {
            case HTTP => result.copy(http = result.http :+ next)
            case HTTPS => result.copy(https = result.https :+ next)
          })
        }

        result
      })
      .log("meta")
      .via(sinkFlow)
      .toMat(sink)(Keep.right)

    // Await.result(graph.run(), 20.seconds)
    Await.ready(graph.run(), Duration.Inf)
    logger.info("finished waiting for graph...")

    Await.result(system.terminate(), 1.seconds)
    logger.info("finished waiting for system termination...")
    System.exit(0)
  }

  def futureResponse(
                      scheme: Scheme,
                      uri: Uri,
                      settings: ConnectionPoolSettings
                    )(implicit system: ActorSystem, mat: Materializer) = {
    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = uri)

    val response: Future[HttpResponse] = Http().singleRequest(request = request, settings = settings)

    val result = response.flatMap(response => {
      response.status match {
        case StatusCodes.OK => {
          response.entity.dataBytes
            .map(bytes => bytes.decodeString(UTF_8))
            .via(new SimpleHTMLParserFlow(scheme, response.status))
            .runWith(Sink.last)
        }
        case invalidCode => Future.successful(Meta(scheme, invalidCode))
      }
    })(system.dispatcher)

    result

  }
}
